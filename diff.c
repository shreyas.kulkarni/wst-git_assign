#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "stack.h"
#define N 1
#define NW 2
#define W 3
int lncount(char file[]);
int linesep(char file1[], char file2[]);
void mylcs(char **x, char **y);

int nol1, nol2;

int main(int argc, char *argv[]) {
	if(argc != 3) {
		printf("Usage: ./project file1 file2\n");
		exit(0);
	}
	else {
	char *file[2];
	file[0] = argv[1];
	file[1] = argv[2];

	nol1 = lncount(file[0]);
	nol2 = lncount(file[1]);
	linesep(file[0], file[1]);
	
	return 0;
	}
}

int linesep(char file1[], char file2[]) {
	FILE *fp1 = NULL;
	FILE *fp2 = NULL;
	char **x, **y;
	ssize_t linelen1, linelen2;
	size_t len1, len2;
	char *ln1 = NULL; 
	char *ln2 = NULL;
	int noc = 2048;
	int i = 0;
	int j = 0;

	fp1 = fopen(file1, "r");
			
	fp2 = fopen(file2, "r");
	
	
	x = (char **)malloc(nol1 * sizeof(char *));
	y = (char **)malloc(nol2 * sizeof(char *));

	while((linelen1 = getline(&ln1, &len1, fp1)) != -1) {
		x[i] = (char *)malloc(noc * sizeof(char));
		x[i] = strdup(ln1);
		i++;
	}
	
	while((linelen2 = getline(&ln2, &len2, fp2)) != -1) {
		y[j] = (char *)malloc(noc * sizeof(char));
		y[j] = strdup(ln2);
		j++;
	}

	mylcs(x,y);

	return 0;
}


int lncount(char file[]) {
	FILE *fp = NULL;
	char *line = NULL;
    size_t linelen = 0;
	ssize_t check;
	int count = 0;

	fp = fopen(file, "r");
	while ((check = getline(&line, &linelen, fp)) > 0)
		count++;

	fclose(fp);
	return count;
}


void mylcs(char **x, char **y) {
	int m, n;
	int i = 0;
	int j = 0;
	m = nol1;
	n = nol2;

	int **num, **dir;

	num = (int **)malloc(sizeof(int *) * (nol1 + 1));
	dir = (int **)malloc(sizeof(int *) * (nol1 + 1));

	for (i = 0; i <= m ; i++) {
		num[i] = (int *)malloc(sizeof(int) * (nol2 + 1));
		dir[i] = (int *)malloc(sizeof(int) * (nol2 + 1));
	} 
	for (j = 0; j <= n; j++) {
		num[0][j] = 0;
		dir[0][j] = W;
	}
	for (i = 0; i <= m; i++) {
		num[i][0] = 0;
		dir[i][0] = N;
	}

	for (i = 1; i <= m; i++) {				//LCS table being created here.
		for(j = 1; j <= n; j++) {
			if(strcmp(x[i-1],y[j-1]) == 0) {
				num[i][j] = num[i-1][j-1] + 1;
				dir[i][j] = NW;
			}
			else if(num[i-1][j] >= num[i][j-1]) {
				num[i][j] = num[i-1][j];
				dir[i][j] = N;
			}
			else {
				num[i][j] = num[i][j-1];
				dir[i][j] = W;
			}
		}
	}
//Getting lcs and diff output by traversing the table
//traversal starts from bottom right corner to top left corner. 
	i = m; 
	j = n;
	nums add, del;
	stack addstack, delstack; 
	stackinit(&addstack);
	stackinit(&delstack);
	while(1) {
		if(i == 0 && j == 0)
			break; 
		if(dir[i][j] == NW) {
			if(!isempty(&delstack) && !isempty(&addstack)) {
				add = pop(&addstack);
				del = pop(&delstack);
				printf("%d", del.i);
				if(stacksize(delstack) > 0)
						printf(",%d", del.i + stacksize(delstack));
				printf("c%d", add.j);
				if(stacksize(addstack) > 0)
					printf(",%d", add.j + stacksize(addstack));
				printf("\n");
				printf("< %s", x[del.i - 1]);
					while(!isempty(&delstack)) {
						del = pop(&delstack);
						printf("< %s", x[del.i - 1]);
					}
				stackinit(&delstack);
				printf("---\n");
				printf("> %s", y[add.j - 1]);
				while(!isempty(&addstack)) {
					add = pop(&addstack);
					printf("> %s", y[add.j - 1]);
				} 
				stackinit(&addstack);
			}
			else if(!isempty(&delstack) && isempty(&addstack)) {
					del = pop(&delstack);
					printf("%d", del.i);
					if(stacksize(delstack) > 0)
						printf(",%d", del.i + stacksize(delstack));
					printf("d%d\n", del.j);
					printf("< %s", x[del.i - 1]);
					while(!isempty(&delstack)) {
						del = pop(&delstack);
						printf("< %s", x[del.i - 1]);
					}
					stackinit(&delstack);		
			}
			else if(isempty(&delstack) && !isempty(&addstack)) {
				add = pop(&addstack);
				printf("%da%d", add.i, add.j);
				if(stacksize(addstack) > 0)
					printf(",%d", add.j + stacksize(addstack));
				printf("\n");
				printf("> %s", y[add.j - 1]);
				while(!isempty(&addstack)) {
					add = pop(&addstack);
					printf("> %s", y[add.j - 1]);
				} 
				stackinit(&addstack);
			}
			i--;
			j--;
		}
		else if(dir[i][j] == W) {
			add.i = i;
			add.j = j;
			push(&addstack, add);
			j--; 
		} 
		else if(dir[i][j] == N) {
			del.i = i;
			del.j = j;
			push(&delstack, del);
			i--; 
		}
	}
	if(!isempty(&delstack) && !isempty(&addstack)) {
		add = pop(&addstack);
		del = pop(&delstack);
		printf("%d", del.i);
		if(stacksize(delstack) > 0)
			printf(",%d", del.i + stacksize(delstack));
		printf("c%d", add.j);
		if(stacksize(addstack) > 0)
			printf(",%d", add.j + stacksize(addstack));
		printf("\n");
		printf("< %s", x[del.i - 1]);
		while(!isempty(&delstack)) {
			del = pop(&delstack);
			printf("< %s", x[del.i - 1]);
		}
		stackinit(&delstack);
		printf("---\n");
		printf("> %s", y[add.j - 1]);
		while(!isempty(&addstack)) {
			add = pop(&addstack);
			printf("> %s", y[add.j - 1]);
		} 
		stackinit(&addstack);
	}
	else if(!isempty(&delstack) && isempty(&addstack)) {
		del = pop(&delstack);
		printf("%d", del.i);
		if(stacksize(delstack) > 0)
			printf(",%d", del.i + stacksize(delstack));
		printf("d%d\n", del.j);
		printf("< %s", x[del.i - 1]);
		while(!isempty(&delstack)) {
			del = pop(&delstack);
			printf("< %s", x[del.i - 1]);
		}
		stackinit(&delstack);
	}
	else if(isempty(&delstack) && !isempty(&addstack)) {
		add = pop(&addstack);
		printf("%da%d", add.i, add.j);
		if(stacksize(addstack) > 0)
			printf(",%d", add.j + stacksize(addstack));
		printf("\n");
		printf("> %s", y[add.j - 1]);
		while(!isempty(&addstack)) {
			add = pop(&addstack);
			printf("> %s", y[add.j - 1]);
		} 
		stackinit(&addstack);
	}

	for(i = 0; i < nol1; i++) {
		free(num[i]);
		free(dir[i]);
	}
	free(num);
	free(dir);	
}

