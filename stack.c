#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
#include <string.h>


void stackinit(stack *q) {
	q->head = NULL;
	q->tail = NULL;
	q->stacksize = 0;
}

int isempty(stack *q) {
	if(q->head == NULL) 
		return 1;
	else return 0;
}

int isfull(stack *q) {
	return 0;
}

void push(stack *q, nums d) {
	if (q->head == NULL) {
		q->head = (node*)malloc(sizeof(node));
		q->head->nums = d;
		q->head->next = q->head;
		q->tail = q->head;
		q->stacksize++;
	}
	else {
		node *temp;
		temp = (node*)malloc(sizeof(node));
		temp->nums = d;
		temp->next = q->head;
		q->head = temp;
		q->tail->next = q->head;
		q->stacksize++;
	}
}	

nums pop(stack *q) {
	node *pointer;
	nums send;
	send = q->head->nums;
	pointer = q->head;

	if (q->head == q->tail) {
	    free(pointer);
		q->head = NULL;
		q->tail = NULL;
		q->stacksize--;
	}

	else {
		pointer = pointer->next;
		free(q->head);
		q->tail->next = pointer;
		q->head = pointer;
		q->stacksize--;
	}
	return send;
}

int stacksize(stack q) {
	return q.stacksize;
}
